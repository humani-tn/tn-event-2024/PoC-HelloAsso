package web

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"poc-helloasso/autogen"
	"poc-helloasso/internal/api"
	"poc-helloasso/internal/config"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func OptionalString(s string) *string {
	if s == "" {
		return nil
	}
	return &s
}

func Setup(g *gin.Engine) {
	conf := config.GetConfig()

	g.GET("/", func(c *gin.Context) {
		c.File("www/index.html")
	})

	g.GET("/don/:streamer", func(c *gin.Context) {
		c.File("www/don.html")
	})

	g.GET("/thanks", func(c *gin.Context) {
		c.File("www/thanks.html")
	})

	g.GET("/error", func(c *gin.Context) {
		c.File("www/error.html")
	})

	type CheckoutForm struct {
		Amount   int32  `json:"amount"`
		Streamer string `json:"streamer"`
	}

	type Redirect struct {
		RedirectURL string `json:"redirectUrl"`
	}

	type OrderData struct {
		Amount struct {
			Total    int32 `json:"total"`
			Vat      int32 `json:"vat"`
			Discount int32 `json:"discount"`
		} `json:"amount"`
	}

	type Notif struct {
		Data      interface{}            `json:"data"`
		EventType string                 `json:"eventType"`
		Metadata  map[string]interface{} `json:"metadata"`
	}

	g.POST("/api/notif", func(c *gin.Context) {
		var notif Notif
		if err := c.ShouldBindJSON(&notif); err != nil {
			c.JSON(400, gin.H{
				"error": err.Error(),
			})
			return
		}

		// Print HEADER + BODY
		h := c.Request.Header

		logrus.Infof("Received notification\nHeaders: %s\nNotif: %+v", fmt.Sprintf("%+v", h), notif)

		if notif.EventType == "Order" {
			data, err := json.Marshal(notif.Data)
			if err != nil {
				c.JSON(500, gin.H{
					"error": err.Error(),
				})
				return
			}
			var order OrderData
			if err := json.Unmarshal(data, &order); err != nil {
				c.JSON(500, gin.H{
					"error": err.Error(),
				})
				return
			}

			// Send to discord using conf.DiscordWebhook
			logrus.Info("Received order: ", fmt.Sprintf("%+v", order))

			amount := fmt.Sprintf("%d", order.Amount.Total)
			amount = amount[:len(amount)-2] + "." + amount[len(amount)-2:]
			streamer, ok := notif.Metadata["streamer"]
			discordMessage := fmt.Sprintf("Don reçu : %s€", amount)
			if ok {
				discordMessage = fmt.Sprintf("Don reçu: %s€ (sur la cagnotte de **%s**)", amount, streamer)
			}
			body := []byte(fmt.Sprintf(`{"content": "%s"}`, discordMessage))
			reader := bytes.NewReader(body)
			resp, err := http.Post(conf.DiscordWebhook, "application/json", reader)
			if err != nil {
				c.JSON(500, gin.H{
					"error": err.Error(),
				})
				return
			}
			if resp.StatusCode != 204 {
				c.JSON(500, gin.H{
					"error": "Discord webhook returned status code " + fmt.Sprintf("%d", resp.StatusCode),
				})
				return
			}
		}
	})

	g.POST("/api/form", func(c *gin.Context) {
		var form CheckoutForm
		if err := c.ShouldBindJSON(&form); err != nil {
			c.JSON(400, gin.H{
				"error": err.Error(),
			})
			return
		}

		token, err := api.Client.GetToken()
		if err != nil {
			c.JSON(500, gin.H{
				"error": err.Error(),
			})
			return
		}

		resp, err := api.Client.OrganizationCheckoutIntentsPostInitCheckout(
			c,
			conf.HelloAssoConfig.Slug,
			&autogen.OrganizationCheckoutIntentsPostInitCheckoutParams{
				Authorization: OptionalString(token.AccessToken),
			},
			autogen.HelloAssoApiV5ModelsCartsInitCheckoutBody{
				BackUrl:          conf.BaseURL,
				ErrorUrl:         conf.BaseURL + "/error",
				ReturnUrl:        conf.BaseURL + "/thanks",
				InitialAmount:    form.Amount,
				TotalAmount:      form.Amount,
				ContainsDonation: true,
				ItemName:         "Donation",
				Metadata: &map[string]interface{}{
					"streamer": form.Streamer,
				},
				TrackingParameter: OptionalString("101"),
			},
		)
		if err != nil {
			c.JSON(500, gin.H{
				"error": err.Error(),
			})
			return
		}

		var bdy Redirect

		// unmarshal resp.Body into bdy
		if err := json.NewDecoder(resp.Body).Decode(&bdy); err != nil {
			c.JSON(500, gin.H{
				"error": err.Error(),
			})
			return
		}

		c.JSON(200, bdy)
	})
}
