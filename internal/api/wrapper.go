package api

import (
	"poc-helloasso/autogen"
	"poc-helloasso/internal/config"

	"github.com/sirupsen/logrus"
)

var Client *autogen.Client

func init() {
	c := config.GetConfig()

	client, err := autogen.NewClient(c.HelloAssoConfig.URL + "/v5")
	if err != nil {
		logrus.Fatal(err)
	}

	_, err = client.GetToken()
	if err != nil {
		logrus.Fatal(err)
	}

	Client = client

	logrus.Info("HelloAsso client initialized")
}
