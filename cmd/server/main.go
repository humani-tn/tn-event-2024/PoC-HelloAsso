package main

import (
	"poc-helloasso/internal/config"
	"poc-helloasso/web"

	"github.com/gin-gonic/gin"
)

func main() {
	e := gin.Default()
	c := config.GetConfig()

	web.Setup(e)

	e.Run(":" + c.Port)
}
